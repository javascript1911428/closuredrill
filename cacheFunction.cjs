function cacheFunction(cb) {
    if (typeof cb !== "function") {
        throw new Error("Invalid parameter.Please provide a callback function.");
    }

    let cache = {};

    return function (...args) {
        if (args.length === 0) {
            return cb();
        }

        const key = JSON.stringify(args);

        if (key in cache) {
            return cache[key];
        }

        let result = cb(...args);
        cache[key] = result;
        return result;
    };
}

module.exports = cacheFunction;