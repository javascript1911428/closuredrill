let cacheFunctionResult = require("./cacheFunction.cjs");

function add(number1, number2) {
    return number1 + number2;
}

let result = cacheFunctionResult(add);

console.log(result(2, 3));