let limitFunctionCallCountFunction = require("./limitFunctionCallCount.cjs");

function wish() {
    return "Hii hello, how are you"
}

let callLimitFunction = limitFunctionCallCountFunction(wish, 4);

console.log(callLimitFunction());

console.log(callLimitFunction());

console.log(callLimitFunction());

console.log(callLimitFunction());

console.log(callLimitFunction());

