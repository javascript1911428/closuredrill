let counterFactoryFunction = require("./counterFactory.cjs");

let result = counterFactoryFunction();

console.log(result.increment());

console.log(result.increment());

console.log(result.decrement());

console.log(result.decrement());
