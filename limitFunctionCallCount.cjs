function limitFunctionCallCount(cb, n) {

    if (typeof cb !== 'function' || typeof n !== "number" || n < 1) {
        throw new Error("Invalid parameters.Please provide a callback function and a positive number");
    }

    let noOfExecutions = 0;

    function executeTheFunction(...args) {
        noOfExecutions = noOfExecutions + 1;

        if (noOfExecutions <= n) {
            return cb(...args);
        } else {
            return null;
        }
    }

    return executeTheFunction;
}

module.exports = limitFunctionCallCount;